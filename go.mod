module gitlab.com/thedevelopnik/configorator

go 1.13

require (
	github.com/fatih/camelcase v1.0.0
	github.com/stretchr/testify v1.4.0
	gopkg.in/yaml.v2 v2.2.2
)
